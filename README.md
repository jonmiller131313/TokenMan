REST API token management.

# Credential Precedence

1. Command line options: `--api-token[=]<TOKEN>`
   - Care must be used to not leak the secret token to the screen/command history
2. Environment variable: `${<PROGRAM>_API_TOKEN}`
3. Local files: `./(../)*<PROGRAM | "config">.toml` (path regex)
   - `config.toml` is only checked in the current directory
4. [XDG Base Directory Specification](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html):
   - `${XDG_CONFIG_HOME}/<PROGRAM>.toml`
   - `${XDG_CONFIG_HOME}/<PROGRAM>/<PROGRAM | "config">.toml`
   - Similarly searching the paths in the `:`-separated list in `${XDG_CONFIG_DIRS}`
5. Finally, fallback to checking `/etc/` similar to the above

## `<PROGRAM>` Format

The format of `<PROGRAM>` should consist of only alphanumeric and underscore characters:

- Spaces and dashes are converted to underscores
- Other special characters are stripped
- Files are lowercase, environment variables are upper case
- Duplicate `_` are reduced
- Panics if the above reduces to an empty string or `_`


# Credential TOML Files

If reading from a [`.toml` file](https://toml.io), the value is stored in `rest_api/token`:

```toml
# ...

[rest_api]
token = "******"

# ...
```

## Permission Warning

A warning is emitted on standard error if the file has any form of global permission access, or any level's executable
bit is set.
