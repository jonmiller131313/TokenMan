use std::env;

use crate::errors::EmptyCLIOption;

pub fn read() -> Result<Option<String>, EmptyCLIOption> {
    let mut argv = env::args();

    match argv.find(|option| option.starts_with("--api-token")) {
        None => return Ok(None),
        Some(option) => match option.split_once('=') {
            None => (),
            Some((_, "")) => return Err(EmptyCLIOption),
            Some((_, token)) => return Ok(Some(token.to_string())),
        },
    }
    return argv.next().ok_or(EmptyCLIOption).map(|token| Some(token));
}
