use std::str::FromStr;
use std::{
    env::current_dir,
    error::Error,
    fs,
    io::ErrorKind::NotFound,
    path::{Path, PathBuf},
};

use toml::{self, Value as TomlValue};
use xdg::BaseDirectories;

use crate::{errors::*, name_format::ProgramName};

pub fn search_relative(unformatted_name: &str) -> Result<Option<String>, TokenError> {
    use OneOrMultipleErrors::*;

    let file_name = ProgramName::file(unformatted_name);
    let file_name = file_name.as_str();
    let mut parent_path = current_dir().expect("Unable to get current working directory?");
    let mut token = None;
    let mut err_chain = ErrorChain::new();

    match check_dir(&parent_path, file_name) {
        Ok(None) => (),
        Ok(Some(token)) => return Ok(Some(token)),
        Err(err) if err.token.is_some() => return Err(err),
        Err(TokenError {
            token: None,
            errors: One(err),
        }) => err_chain.push(err),
        Err(TokenError {
            token: None,
            errors: Multiple(chain),
        }) => err_chain = chain,
        _ => panic!("Not reachable"),
    }

    while parent_path.pop() {
        match read_file(&parent_path, file_name) {
            Ok(Some(t)) => {
                token = Some(t);
                break;
            }
            Ok(None) => (),
            Err(err) => err_chain.push(err),
        }
    }

    return match err_chain.len() {
        0 => Ok(token),
        1 => Err(TokenError {
            token,
            errors: One(err_chain.pop().unwrap()),
        }),
        _ => Err(TokenError {
            token,
            errors: Multiple(err_chain),
        }),
    };
}

pub fn search_xdg(unformatted_name: &str) -> Result<Option<String>, TokenError> {
    use OneOrMultipleErrors::*;

    let file_name = ProgramName::file(unformatted_name);
    let file_name = file_name.as_str();
    let mut err_chain = ErrorChain::new();

    let xdg = BaseDirectories::new().expect("Unable to read XDG base directories?");
    let xdg_prefixed = BaseDirectories::with_prefix(file_name).expect("Unable to read XDG base directories?");

    // Maintain order of preference, but zip with a prefixed version
    // e.g. ("~/.config/", "~/.config/program_name")
    let config_dirs: Vec<(PathBuf, PathBuf)> = [(xdg.get_config_home(), xdg_prefixed.get_config_home())]
        .into_iter()
        .chain(
            xdg.get_config_dirs()
                .into_iter()
                .zip(xdg_prefixed.get_config_dirs().into_iter()),
        )
        .collect();

    for (base_dir, prefixed_dir) in config_dirs.iter() {
        match read_file(base_dir, file_name) {
            Ok(Some(t)) => return Ok(Some(t)),
            Ok(None) => (),
            Err(e) => err_chain.push(e),
        }

        let token;
        match check_dir(prefixed_dir, file_name) {
            Ok(res) => token = res,
            Err(TokenError { token: t, errors }) => {
                token = t;
                match errors {
                    One(e) => err_chain.push(e),
                    Multiple(e) => err_chain.extend(e.into_iter()),
                }
            }
        }
        match (token, err_chain.len()) {
            (None, _) => (),
            (Some(token), 0) => return Ok(Some(token)),
            (Some(token), 1) => return Err(TokenError::soft_with_error(token, err_chain.pop().unwrap())),
            (Some(token), _) => return Err(TokenError::soft_with_errors(token, err_chain)),
        }
    }

    return Ok(None);
}

pub fn search_etc(unformatted_name: &str) -> Result<Option<String>, TokenError> {
    use OneOrMultipleErrors::*;

    let file_name = ProgramName::file(unformatted_name);
    let file_name = file_name.as_str();
    let etc = PathBuf::from_str("/etc").unwrap();
    let mut err_chain = ErrorChain::new();

    match read_file(&etc, file_name) {
        Ok(Some(t)) => return Ok(Some(t)),
        Ok(None) => (),
        Err(e) => err_chain.push(e),
    }
    let token;
    match check_dir(&etc.join(file_name), file_name) {
        Ok(res) => token = res,
        Err(TokenError { token: t, errors }) => {
            token = t;
            match errors {
                One(e) => err_chain.push(e),
                Multiple(e) => err_chain.extend(e.into_iter()),
            }
        }
    }
    return match err_chain.len() {
        0 => Ok(token),
        1 => {
            return Err(TokenError {
                token,
                errors: One(err_chain.pop().unwrap()),
            })
        }
        _ => {
            return Err(TokenError {
                token,
                errors: Multiple(err_chain),
            })
        }
    };
}

fn check_dir(directory: &Path, program_config_file: &str) -> Result<Option<String>, TokenError> {
    let mut err_chain = ErrorChain::new();

    match read_file(directory, "config") {
        Ok(None) => (),
        Ok(Some(token)) => return Ok(Some(token)),
        Err(err) => err_chain.push(err),
    }

    let token = read_file(directory, program_config_file).unwrap_or_else(|err| {
        err_chain.push(err);
        None
    });

    return match err_chain.len() {
        0 => Ok(token),
        1 => Err(TokenError {
            token,
            errors: OneOrMultipleErrors::One(err_chain.pop().unwrap()),
        }),
        _ => Err(TokenError {
            token,
            errors: OneOrMultipleErrors::Multiple(err_chain),
        }),
    };
}

fn read_file(parent_path: &Path, file_name: &str) -> Result<Option<String>, Box<dyn Error + Send + Sync>> {
    let config_file_path = parent_path.join(file_name.to_string() + ".toml");
    let file_contents = match fs::read_to_string(&config_file_path) {
        Ok(contents) => contents,
        Err(err) if err.kind() == NotFound => return Ok(None),
        Err(err) => return Err(Box::new(ConfigIOError::new(config_file_path, err))),
    };
    let toml_data: TomlValue =
        toml::from_str(&file_contents).map_err(|err| Box::new(InvalidToml::new(config_file_path.clone(), err)))?;

    let create_missing_token_err = || {
        Box::new(FileMissingToken {
            file: config_file_path.clone(),
        })
    };
    let token_value = toml_data
        .get("rest_api")
        .ok_or_else(create_missing_token_err)?
        .get("token")
        .ok_or_else(create_missing_token_err)?;

    return match token_value {
        TomlValue::String(token) => Ok(Some(token.clone())),
        _ => Err(Box::new(TokenNotString {
            converted: toml::to_string(token_value).expect("Unable to convert raw token value to string?"),
            file: config_file_path,
        })),
    };
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn relative_search() {
        let _ = search_relative("test-lib");
    }

    #[test]
    fn xdg_search() {
        let _ = search_xdg("test-lib");
    }

    #[test]
    fn etc_search() {
        let _ = search_etc("test-lib");
    }
}
