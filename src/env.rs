use std::env;

use crate::name_format::ProgramName;

pub fn read(unformatted_name: &str) -> Option<String> {
    match env::var(ProgramName::envar(unformatted_name)) {
        Ok(token) => Some(token),
        Err(env::VarError::NotPresent) => None,
        Err(env::VarError::NotUnicode(_)) => {
            unimplemented!("non-unicode environment variables not supported")
        }
    }
}
