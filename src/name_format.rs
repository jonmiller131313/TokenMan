use std::ffi::OsStr;

#[derive(Debug)]
enum ProgramNameFormat {
    EnvironmentVariable,
    ConfigFile,
}
#[derive(Debug)]
pub struct ProgramName(String);
impl ProgramName {
    pub fn envar(s: &str) -> Self {
        Self::formatted(s, ProgramNameFormat::EnvironmentVariable)
    }

    pub fn file(s: &str) -> Self {
        Self::formatted(s, ProgramNameFormat::ConfigFile)
    }

    pub fn as_str(&self) -> &str {
        self.0.as_str()
    }

    fn formatted(s: &str, format: ProgramNameFormat) -> Self {
        use ProgramNameFormat::*;
        const UNDERSCORE_CHARS: [char; 2] = ['_', '-'];
        let formatted = s
            .chars()
            .filter_map(|ch| {
                return if UNDERSCORE_CHARS.contains(&ch) {
                    Some('_')
                } else if !ch.is_alphanumeric() {
                    None
                } else {
                    match &format {
                        EnvironmentVariable => Some(ch.to_ascii_uppercase()),
                        ConfigFile => Some(ch.to_ascii_lowercase()),
                    }
                };
            })
            .collect::<String>()
            .replace("__", "_");
        debug_assert!(!formatted.is_empty(), "Program name cannot be empty");
        debug_assert_ne!(formatted, "_", "Formatted program name cannot be \"_\"");
        return Self(
            formatted
                + match format {
                    EnvironmentVariable => "_API_TOKEN",
                    ConfigFile => "",
                },
        );
    }
}
impl AsRef<str> for ProgramName {
    fn as_ref(&self) -> &str {
        self.0.as_str()
    }
}
impl AsRef<OsStr> for ProgramName {
    fn as_ref(&self) -> &OsStr {
        self.0.as_ref()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    // Test case, environment variable, config file
    const CASES: [(&str, &str, &str); 4] = [
        ("test", "TEST_API_TOKEN", "test"),
        ("test-program", "TEST_PROGRAM_API_TOKEN", "test_program"),
        (
            r"test-\w-special-char",
            "TEST_W_SPECIAL_CHAR_API_TOKEN",
            "test_w_special_char",
        ),
        ("test's_program", "TESTS_PROGRAM_API_TOKEN", "tests_program"),
    ];

    #[test]
    fn environment_variables() {
        for (input, envar, _) in CASES {
            let formatted = ProgramName::envar(input);
            assert_eq!(formatted.as_str(), envar)
        }
    }

    #[test]
    fn config_file() {
        for (input, _, file) in CASES {
            let formatted = ProgramName::file(input);
            assert_eq!(formatted.as_str(), file)
        }
    }

    #[test]
    #[should_panic(expected = "Program name cannot be empty")]
    fn panic_empty() {
        let _ = ProgramName::envar("");
    }

    #[test]
    #[should_panic(expected = "Formatted program name cannot be \"_\"")]
    fn panic_underscore() {
        let _ = ProgramName::file("-");
    }
}
