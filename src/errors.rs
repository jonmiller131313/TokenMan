//! Error handling types.
//!
//! [`TokenError`] is the top level error type. It's able to "soft" error where we do find a valid token, but
//! encountered errors while doing so.
use std::{error::Error, io::Error as IOError, path::PathBuf};

use thiserror::Error;
use toml::de::Error as TomlError;

/// An error occurred while searching for a token.
#[derive(Debug, Error)]
#[error("encountered error(s) while searching for a token")]
pub struct TokenError {
    /// `Some` if we were able to find a token.
    pub token: Option<String>,
    /// The error(s) encountered.
    pub errors: OneOrMultipleErrors,
}
impl TokenError {
    pub(crate) fn soft_with_error(token: String, error: Box<dyn Error + Send + Sync>) -> Self {
        Self {
            token: Some(token),
            errors: OneOrMultipleErrors::One(error),
        }
    }

    pub(crate) fn soft_with_errors(token: String, errors: ErrorChain) -> Self {
        Self {
            token: Some(token),
            errors: OneOrMultipleErrors::Multiple(errors),
        }
    }
}

/// Multiple errors occurred when searching for the token.
pub type ErrorChain = Vec<Box<dyn Error + Send + Sync>>;
impl From<ErrorChain> for TokenError {
    fn from(mut chain: ErrorChain) -> Self {
        let token = None;
        let errors = match chain.len() {
            0 => OneOrMultipleErrors::One(Box::new(NoTokenFound)),
            1 => OneOrMultipleErrors::One(chain.pop().unwrap()),
            _ => OneOrMultipleErrors::Multiple(chain),
        };
        return TokenError { token, errors };
    }
}

/// If one or multiple errors occurred.
#[derive(Debug)]
pub enum OneOrMultipleErrors {
    /// Only one error.
    One(Box<dyn Error + Send + Sync>),
    /// Multiple errors.
    Multiple(ErrorChain),
}

/// No value given on the command line.
#[derive(Debug, Error)]
#[error("no value given on the command line")]
pub struct EmptyCLIOption;

/// An IO error occurred while trying to read a config file.
#[derive(Debug, Error)]
#[error("Unable to read config file {file}")]
pub struct ConfigIOError {
    /// The config file.
    pub file: PathBuf,

    #[source]
    source: IOError,
}
impl ConfigIOError {
    pub(crate) fn new(file: PathBuf, source: IOError) -> Self {
        Self { file, source }
    }
}

/// Invalid TOML in the config file.
#[derive(Debug, Error)]
#[error("file {file} has invalid TOML")]
pub struct InvalidToml {
    /// The config file.
    pub file: PathBuf,

    #[source]
    source: TomlError,
}
impl InvalidToml {
    pub(crate) fn new(file: PathBuf, source: TomlError) -> Self {
        Self { file, source }
    }
}

/// Missing token key in a config file.
#[derive(Debug, Error)]
#[error("file {file} does not contain the rest_api/token key")]
pub struct FileMissingToken {
    /// The config file.
    pub file: PathBuf,
}

/// The token value in the config file is not a string.
#[derive(Debug, Error)]
#[error("token in file {file} is not a string")]
pub struct TokenNotString {
    /// The value converted into a string.
    pub converted: String,
    /// The config file.
    pub file: PathBuf,
}

/// If we exhaust all options and no other error occurred.
#[derive(Debug, Error)]
#[error("no token found")]
pub struct NoTokenFound;
