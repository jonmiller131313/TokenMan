#![doc = include_str!("../README.md")]
#![warn(missing_docs)]

use std::fmt::{Debug, Formatter};

use crate::errors::{ErrorChain, TokenError};

mod cli;
mod env;
pub mod errors;
mod files;
mod name_format;

/// A token read from a CLI option, environment variable, or configuration file.
pub struct Token(String);
impl Token {
    /// Searches for a token by formatting the program's name.
    pub fn search(program_name: &str) -> Result<Self, TokenError> {
        use errors::OneOrMultipleErrors::*;

        let mut err_chain = ErrorChain::new();

        match cli::read() {
            Ok(None) => (),
            Ok(Some(token)) => return Ok(Self(token)),
            Err(err) => err_chain.push(Box::new(err)),
        }

        if let Some(token) = env::read(program_name) {
            if let Some(err) = err_chain.pop() {
                return Err(TokenError::soft_with_error(token, err));
            }
            return Ok(Self(token));
        }

        let token;
        match files::search_relative(program_name) {
            Ok(res) => token = res,
            Err(TokenError { token: t, errors }) => {
                token = t;
                match errors {
                    One(e) => err_chain.push(e),
                    Multiple(errs) => err_chain.extend(errs.into_iter()),
                }
            }
        }
        if let Some(token) = token {
            return match err_chain.len() {
                0 => Ok(Self(token)),
                1 => Err(TokenError::soft_with_error(token, err_chain.pop().unwrap())),
                _ => Err(TokenError::soft_with_errors(token, err_chain)),
            };
        }

        let token;
        match files::search_xdg(program_name) {
            Ok(res) => token = res,
            Err(TokenError { token: t, errors }) => {
                token = t;
                match errors {
                    One(e) => err_chain.push(e),
                    Multiple(errs) => err_chain.extend(errs.into_iter()),
                }
            }
        }
        if let Some(token) = token {
            return match err_chain.len() {
                0 => Ok(Self(token)),
                1 => Err(TokenError::soft_with_error(token, err_chain.pop().unwrap())),
                _ => Err(TokenError::soft_with_errors(token, err_chain)),
            };
        }

        let token;
        match files::search_etc(program_name) {
            Ok(res) => token = res,
            Err(TokenError { token: t, errors }) => {
                token = t;
                match errors {
                    One(e) => err_chain.push(e),
                    Multiple(errs) => err_chain.extend(errs.into_iter()),
                }
            }
        }
        if let Some(token) = token {
            return match err_chain.len() {
                0 => Ok(Self(token)),
                1 => Err(TokenError::soft_with_error(token, err_chain.pop().unwrap())),
                _ => Err(TokenError::soft_with_errors(token, err_chain)),
            };
        }

        return Err(TokenError::from(err_chain));
    }

    /// Accesses the stored token.
    pub fn as_str(&self) -> &str {
        self.0.as_str()
    }
}
impl Debug for Token {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "*****")
    }
}
impl AsRef<str> for Token {
    fn as_ref(&self) -> &str {
        self.0.as_str()
    }
}
impl From<Token> for String {
    fn from(token: Token) -> Self {
        token.0
    }
}
